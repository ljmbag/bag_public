# Ljm Log Library

shopt  -s  expand_aliases  # need for alias
alias log='log_info'	# default: log_info

alias log_info='_log_info $LINENO '
_log_info() {
	echo -e "\033[46;37;1m[$0 Line:$1] \033[0m\033[47;30;2m$2\033[0m"
}

alias log_alert='_log_alert $LINENO '
_log_alert() {
	echo -e "\033[103;37;1m\nALERT   [$0:${FUNCNAME[1]} Line:$1]\033[0m\033[107;30;2m\n$2\033[0m"
}

alias log_success='_log_success $LINENO '
_log_success() {
	echo -e "\033[42;37;1m\nSUCCESS [$0:${FUNCNAME[1]} Line:$1]\033[0m\033[107;30;2m\n$2\033[0m"
}

alias log_error='_log_error $LINENO '
_log_error() {
	echo -e "\033[41;37;1m\nERROR   [$0:${FUNCNAME[1]} Line:$1]\033[0m\033[107;30;2m\n$2\033[0m" >&2
}
