# bash command parse library
# API:
#   configure:
#	arg_optional_single "long variable_name" "short variable_name" "required"/"optional"  "help message" "default value"	
#	arg_boolean_single "long variable_name" "short variable_name" "required"/"optional"  "help message" "default value" 
#	arg_positional_single "variable_name" "help message" "default value"
#	arg_help "help message of script"
#   begin parse:
#	arg_go "$@"
#   print parse result:
#	arg_print
# 
# 

arg_optional_single()
{
	# input check
	[ $# -gt 5 ] && die "arg_optional_single($1|$2) need 5 or less inputs" 1
	[[ $1 == "" ]] && die "arg_optional_single($1|$2) input error" 1

	# help add. Input $arg_usage_help_message $arg_argument_help_message
	[[ $1 != "" ]] && [[ $2 != "" ]] && arg_usage_help_message="$arg_usage_help_message [-$2|--$1 <arg>]"
	[[ $1 != "" ]] && [[ $1 == "" ]] && arg_usage_help_message="$arg_usage_help_message [--$1 <arg>]"
	
	# required OR optional?
	arg_require_message=$3
	[[ $arg_require_message == "" ]] && arg_require_message='optional'
	[[ $arg_require_message == "Optional" ]] && arg_require_message='optional'
	[[ $arg_require_message == "Required" ]] && arg_require_message='required'
	[[ $arg_require_message != "required" && $arg_require_message != "optional" ]] && die "arg_optional_single($1|$2) input error. \$3 should be required or optional" 1 

	[[ $1 != "" ]] && [[ $2 != "" ]] && arg_argument_help_message="$arg_argument_help_message		printf '\t%s\n' \"-$2, --$1: $1 [$arg_require_message], $4 (default: '$5')\"
"
	[[ $1 != "" ]] && [[ $2 == "" ]] && arg_argument_help_message="$arg_argument_help_message		printf '\t%s\n' \"--$1: $1 [$arg_require_message], $4 (default: '$5')\"
"

	# begins_with_short_option add. $arg_short_option_all
	[[ $2 != "" ]] && arg_short_option_all="${arg_short_option_all}$2"

	# parse_commandLine add. $arg_parse_rule
	[[ $1 != "" ]] && [[ $2 != "" ]] && arg_parse_rule="${arg_parse_rule}			-$2|--$1)
				test \$# -lt 2 && die \"Missing value for the optional argument '\$_key'.\" 1
				_arg_$1=\"\$2\"
				shift
				;;
			--$1=*)
				_arg_$1=\"\${_key##--$1=}\"
				;;
			-$2*)
				_arg_$1=\"\${_key##-$2}\"
				;;
"
	[[ $1 != "" ]] && [[ $2 == "" ]] && arg_parse_rule="$arg_parse_rule			--$1)
				test \$# -lt 2 && die \"Missing value for the optional argument '\$_key'.\" 1
				_arg_$1=\"\$2\"
				shift
				;;
			--$1=*)
				_arg_$1=\"\${_key##--$1=}\"
				;;
"
	# initial add
        arg_init="${arg_init}_arg_$1=\"$5\"
"

	# input handle add. $arg_require_message
	arg_input_handle="$arg_input_handle
$1=\$_arg_$1"
[[ $arg_require_message == "required" ]] && arg_input_handle="$arg_input_handle
[[ \$$1 == \"\" ]] && die \"$1 should not be empty\" 1"

	# print ans
	arg_print_ans="$arg_print_ans
echo \"$1 = \\\"\$$1\\\"\"
"
}

arg_optional_boolean()
{
	# input check
	[ $# -gt 5 ] && die "arg_optional_boolean($1|$2) need 5 or less inputs" 1
	[[ $1 == "" ]] && die "arg_optional_boolean($1|$2) input error" 1

	# help add
	[[ $1 != "" ]] && [[ $1 != "" ]] && arg_usage_help_message="$arg_usage_help_message [-$2|--$1 <arg>]"
	[[ $1 != "" ]] && [[ $1 == "" ]] && arg_usage_help_message="$arg_usage_help_message [--$1 <arg>]"

	arg_require_message=$3
	[[ $arg_require_message == "" ]] && arg_require_message='optional'
	[[ $arg_require_message == "Optional" ]] && arg_require_message='optional'
	[[ $arg_require_message == "Required" ]] && arg_require_message='required'
	[[ $arg_require_message != "required" && $arg_require_message != "optional" ]] && die "arg_optional_boolean($1|$2) input error. \$3 should be required or optional" 1 

	arg_boolean_default=$5
	[[ $5 == "" ]] && arg_boolean_default="off"

	[[ $1 != "" ]] && [[ $2 != "" ]] && arg_argument_help_message="$arg_argument_help_message		printf '\t%s\n' \"-$2, --$1: $1 [$arg_require_message], $4 (default: '$arg_boolean_default')\"
"	
	[[ $1 != "" ]] && [[ $2 == "" ]] && arg_argument_help_message="$arg_argument_help_message		printf '\t%s\n' \"--$1: $1 [$arg_require_message], $4 (default: '$arg_boolean_default')\"
"

	# begins_with_short_option add
	[[ $2 != "" ]] && arg_short_option_all="${arg_short_option_all}$2"

	# parse_commandLine add
	# parse_commandLine add. $arg_parse_rule
	[[ $1 != "" ]] && [[ $2 != "" ]] && arg_parse_rule="$arg_parse_rule			-$2|--no-$1|--$1)
				_arg_$1=\"on\"
				test \"\${1:0:5}\" = \"--no-\" && _arg_$1=\"off\"
				;;
			-$2*)
				_arg_$1=\"on\"
				_next=\"\${_key##-$2}\"
				if test -n \"\$_next\" -a \"\$_next\" != \"\$_key\"
				then
					{ begins_with_short_option \"\$_next\" && shift && set -- \"-$2\" \"-\${_next}\" \"\$@\"; } || die \"The short option '\$_key' can't be decomposed to \${_key:0:2} and -\${_key:2}, because \${_key:0:2} doesn't accept value and '-\${_key:2:1}' doesn't correspond to a short option.\"
				fi
				;;
"
	[[ $1 != "" ]] && [[ $2 == "" ]] && arg_parse_rule="$arg_parse_rule			--no-$1|--$1)
				_arg_show=\"on\"
				test \"\${1:0:5}\" = \"--no-\" && _arg_show=\"off\"
				;;
"
	# initial add
        [[ $5 == "" ]] && arg_init="${arg_init}_arg_$1=\"off\"
"
        [[ $5 != "" ]] && arg_init="${arg_init}_arg_$1=\"$5\"
"
	
	# input handle add. $arg_require_message
	arg_input_handle="$arg_input_handle
$1=\$_arg_$1"
[[ $arg_require_message == "required" ]] && arg_input_handle="$arg_input_handle
[[ \$$1 == \"\" ]] && die \"$1 should not be empty\" 1"


	# print ans
	arg_print_ans="$arg_print_ans
echo \"$1 = \\\"\$$1\\\"\"
"
}

arg_positional_single()
{
	# input check
	[ $# -gt 3 ] && die "arg_positional_single($1) need 3 or less inputs" 1
	[[ $1 == "" ]] && die "arg_positional_single($1) input error" 1

	# help add
	[[ $1 != "" ]] && [[ $2 != "" ]] && arg_usage_help_message="$arg_usage_help_message [<$1>]"

	arg_argument_help_message="$arg_argument_help_message		printf '\t%s\n' \"<$1>: $2 (default: '$3')\"
"

	# assign_positional_args add
	assign_positional_args_command="assign_positional_args()
{
	local _positional_name _shift_for=\$1
	_positional_names=\"_arg_$1 \"

	shift \"\$_shift_for\"
	for _positional_name in \${_positional_names}
	do
		test \$# -gt 0 || break
		eval \"\$_positional_name=\\\${1}\" || die \"Error during argument parsing, possibly an Argbash bug.\" 1
		shift
	done
}
"
	echo "$assign_positional_args_command" >> tmp.sh
	eval "$assign_positional_args_command"

	arg_positional_arg_exist="True"

	# initial add
	arg_init="${arg_init}_arg_$1=\"$3\"
"
	# input handle
	arg_input_handle="$arg_input_handle
$1=\$_arg_$1"

	# print ans
	arg_print_ans="$arg_print_ans
echo \"$1 = \\\"\$$1\\\"\"
"
}

arg_help() 
{
	# TODO default help
	arg_script_help_message=$1

}

arg_print()
{
	eval "$arg_print_ans"	
}

# exist function zone
die()
{
	local _ret="${2:-1}"
	test "${_PRINT_HELP:-no}" = yes && print_help >&2
	echo "$1" >&2
	exit "${_ret}"
}

handle_passed_args_count()
{
	test "${_positionals_count}" -le 1 || _PRINT_HELP=yes die "FATAL ERROR: There were spurious positional arguments --- we expect between 0 and 1, but got ${_positionals_count} (the last one was: '${_last_positional}')." 1
}

# initial zone  TODO   begin with none   $arg_init
_positionals=()
arg_script_help_message="default script help message."

# go
arg_go(){
	# begins_with_short_option zone. Input $arg_short_option_all
	begins_with_short_option_command="begins_with_short_option()
{
	local first_option all_short_options='${arg_short_option_all}h'
	first_option=\"\${1:0:1}\"
	test \"\$all_short_options\" = \"\${all_short_options/\$first_option/}\" && return 1 || return 0
}
"

	# help zone. Input $arg_script_help_message $arg_usage_help_message $arg_argument_help_message
	help_command="print_help()
{
		printf '%s\n' \"$arg_script_help_message\"
		printf 'Usage: %s [-h|--help]$arg_usage_help_message\n' \"\$0\"
		printf '\t%s\n' \"-h, --help: Prints help\"
$arg_argument_help_message
}
"

	# parse_commandLine zone. Input $arg_parse_rule 
	parse_command="parse_commandline()
{
	_positionals_count=0
	while test \$# -gt 0
	do
		_key=\"\$1\"
		case \"\$_key\" in
$arg_parse_rule
			-h|--help)
				print_help
				exit 0
				;;
			-h*)
				print_help
				exit 0
				;;
			*)
				_last_positional=\"\$1\"
				_positionals+=(\"\$_last_positional\")
				_positionals_count=\$((_positionals_count + 1))
				;;
		esac
		shift
	done
}
"


	# run zone
	#rm tmp.sh
	#touch tmp.sh

	eval "$arg_init"
	#echo "$arg_init" >> tmp.sh
	eval "$begins_with_short_option_command"
	#echo  "$begins_with_short_option_command" >> tmp.sh
	eval "$help_command"
	#echo "$help_command" >> tmp.sh
	eval "$parse_command"
	#echo "$parse_command" >> tmp.sh


	parse_commandline "$@"
	if [[ $arg_positional_arg_exist == "True" ]]
	then
		handle_passed_args_count
		assign_positional_args 1 "${_positionals[@]}"
	fi	
	
	eval "$arg_input_handle"
	#echo "$arg_input_handle" >> tmp.sh
}