#/usr/bin/env bash

git add .
git status

commit_message=$1
if [ -z "$commit_message" ];
then
	echo -n "请输入commit的备注信息: "
	read commit_message
else
	echo -n "按 enter 继续或 ctrl-c 取消:"
	read anyinput
fi

git commit -m "$commit_message"
git push
